/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        bkg: "rgb(var(--color-bkg))",
        content: "rgb(var(--color-content))",
        accent: {
          pink: "rgb(var(--color-accent-pink))",
          yellow: "rgb(var(--color-accent-yellow))",
          lavender: "rgb(var(--color-accent-lavender))",
          orange: "rgb(var(--color-accent-orange))",
        },
        context: {
          error: "rgb(var(--color-context-error))",
          success: "rgb(var(--color-context-success))",
          info: "rgb(var(--color-context-info))",
          warning: "rgb(var(--color-context-warning))",
        },
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
    },
  },
  plugins: [],
};
