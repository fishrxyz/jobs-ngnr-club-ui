const createNewListing = async (listingData) => {
  const url = `${process.env.NEXT_PUBLIC_API_URL}/v1/listings`;
  const res = await fetch(url, {
    method: "POST",
    body: JSON.stringify(listingData),
  });
  return res;
};

export default createNewListing;
