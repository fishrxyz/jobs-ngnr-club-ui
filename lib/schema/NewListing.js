import * as z from "zod";

const NewListingSchema = z.object({
  title: z.string().nonempty("title is required "),
  location: z.string().nonempty("location is required "),
  remote: z.coerce.boolean(),
  pinned: z.coerce.boolean(),
  contract_type: z.string().nonempty(),
  category: z.string().nonempty(),
  description: z.string(),
  posting_url: z.string().nonempty(),
  salary_min: z.coerce.number().nonnegative("Salary cannot be negative"),
  salary_max: z.coerce.number().nonnegative("Salary cannot be negative"),
  company: z.object({
    name: z.string("Please add your company name"),
    size: z.string().nonempty("Company size is required"),
    email: z.string().email(),
    about: z.string().nonempty("Please add a short company description"),
    twitter: z.string(),
    instagram: z.string(),
    linkedin: z.string(),
  }),
});

export default NewListingSchema;
