const NewListingDefaultValues = {
  title: "",
  location: "",
  remote: false,
  pinned: false,
  contract_type: "",
  category: "",
  description: "",
  posting_url: "",
  salary_min: 0,
  salary_max: 0,
  company: {
    name: "",
    size: "",
    email: "",
    about: "",
    twitter: "",
    instagram: "",
    linkedin: "",
  },
};

export default NewListingDefaultValues;
