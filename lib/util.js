const isNestedError = (obj) => Object.keys(obj).length > 1;
const isEmpty = (arr) => arr.length === 0;

export const prettifyErrors = (errors) => {
  delete errors["_errors"];
  for (const err in errors) {
    const fieldName = err;
    const fieldObj = errors[fieldName];

    const message = fieldObj._errors;

    if (isNestedError(fieldObj)) {
      prettifyErrors(fieldObj);
    }
    if (!isEmpty(message)) {
      fieldObj["message"] = message[0];
    }
    delete fieldObj["_errors"];
  }
  console.log(errors);
  return errors;
};
