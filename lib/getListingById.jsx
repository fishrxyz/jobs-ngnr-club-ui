import { notFound } from "next/navigation";
import { DEFAULT_CACHE_DURATION } from "./constants";
import InternalServerError from "./errors/ApiError";

const getListingById = async (publicId) => {
  const url = `${process.env.NEXT_PUBLIC_API_URL}/v1/listings/${publicId}`;
  const nextOptions = {
    next: {
      revalidate: DEFAULT_CACHE_DURATION, // 5 hours
    },
  };
  const res = await fetch(url, nextOptions);

  if (!res.ok) {
    if (res.status === 404 || res.status == 403) {
      return notFound();
    } else {
      throw new InternalServerError(`Internal Server Error: ${res.status}`);
    }
  }

  return res.json();
};

export default getListingById;
