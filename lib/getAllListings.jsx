import { TAG_ALL_LISTINGS, DEFAULT_CACHE_DURATION } from "./constants";
import InternalServerError from "./errors/ApiError";

const getAllListings = async () => {
  const url = `${process.env.NEXT_PUBLIC_API_URL}/v1/listings`;
  const nextOptions = {
    next: {
      revalidate: DEFAULT_CACHE_DURATION, // 5 hours
      tags: [TAG_ALL_LISTINGS],
    },
  };
  const res = await fetch(url, nextOptions);
  if (!res.ok) {
    throw new InternalServerError("Failed to fetch job listings");
  }

  return res.json();
};

export default getAllListings;
