class InternalServerError extends Error {
  constructor(...params) {
    super(...params);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ServerError);
    }

    this.name = "InternalServerError";
    this.status = statusCode;
  }
}

export default InternalServerError;
