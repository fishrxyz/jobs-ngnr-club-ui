"use client";

import { useFormStatus } from "react-dom";

const SubmitJobListingButton = () => {
  const { pending } = useFormStatus();

  return (
    <button type="submit" className="bg-accent-pink text-lg">
      <span className="text-content">
        {pending ? "Posting job ... " : "Post Job for $199"}
      </span>
    </button>
  );
};

export default SubmitJobListingButton;
