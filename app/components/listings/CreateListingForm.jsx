"use client";

import { useRouter } from "next/navigation";
import SubmitJobListingButton from "./SubmitJobListingButton";
import createJobListing from "../../listings/create/actions";
import { useFormState } from "react-dom";
import { useEffect } from "react";

export default function CreateListingForm() {
  const router = useRouter();
  const [state, formAction] = useFormState(createJobListing, null);

  useEffect(() => {
    if (state?.success) {
      return router.push("/");
    }
  }, [router, state?.success]);

  return (
    <form className="text-content" action={formAction}>
      <div className="text-black">
        <label htmlFor="title">
          <span>Job Title</span>
          <input name="title" type="text" />
          {state?.error?.title && (
            <span className="text-red-500">{state?.error.title.message}</span>
          )}
        </label>
        <br />
        <label htmlFor="location">
          <span>Location</span>
          <input name="location" type="text" />
          {state?.error?.location && (
            <span className="text-red-500">
              {state?.error.location.message}
            </span>
          )}
        </label>
        <br />
        <label htmlFor="remote">
          <span>This is a remote position</span>
          <input
            name="remote"
            type="checkbox"
            defaultChecked={false}
            defaultValue={false}
          />
        </label>
        <label htmlFor="pinned">
          <span>This is a remote position</span>
          <input
            name="pinned"
            type="checkbox"
            defaultChecked={false}
            defaultValue={false}
          />
        </label>
        <br />
        <label htmlFor="contract_type">
          <span>Contract Type</span>
          <input name="contract_type" type="text" />
          {state?.error?.contract_type && (
            <span className="text-red-500">
              {state?.error.contract_type.message}
            </span>
          )}
        </label>
        <br />
        <label htmlFor="category">
          <span>Category</span>
          <input name="category" type="text" />
          {state?.error?.category && (
            <span className="text-red-500">
              {state?.error.category.message}
            </span>
          )}
        </label>
        <br />
        <label htmlFor="description">
          <span>Describe the job</span>
          <input name="description" type="text" />
          {state?.error?.descritpion && (
            <span className="text-red-500">
              {state?.error.descritpion.message}
            </span>
          )}
        </label>
        <br />
        <label htmlFor="posting_url">
          <span>Link to job ad</span>
          <input name="posting_url" type="text" />
          {state?.error?.posting_url && (
            <span className="text-red-500">
              {state?.error.posting_url.message}
            </span>
          )}
        </label>
        <br />
        <div>
          <span>Salary Range</span>
          <label htmlFor="salary_min">
            <span>Minimum</span>
            <input name="salary_min" type="number" />
            {state?.error?.salary_min && (
              <span className="text-red-500">
                {state?.error.salary_min.message}
              </span>
            )}
          </label>
          <br />
          <label htmlFor="salary_max">
            <span>Maximum</span>
            <input name="salary_max" type="number" />
            {state?.error?.salary_max && (
              <span className="text-red-500">
                {state?.error.salary_max.message}
              </span>
            )}
          </label>
          <br />
        </div>
        <br />
        <br />
        <br />
        <label htmlFor="company_name">
          <span>Company Name</span>
          <input name="company.name" type="text" />
          {state?.error?.company?.name && (
            <span className="text-red-500">
              {state?.error.company.name.message}
            </span>
          )}
        </label>
        <br />
        <label htmlFor="company_size">
          <span>Company Size</span>
          <input name="company.size" type="text" />
          {state?.error?.company?.size && (
            <span className="text-red-500">
              {state?.error.company.size.message}
            </span>
          )}
        </label>
        <br />
        <label htmlFor="company_website">
          <span>Company Website</span>
          <input name="company_website" type="text" />
          {state?.error?.company?.website && (
            <span className="text-red-500">
              {state?.error.company.website.message}
            </span>
          )}
        </label>
        <br />
        <label htmlFor="company_email">
          <span>Your email (will only be used for billing)</span>
          <input name="company.email" type="text" />
          {state?.error?.company?.email && (
            <span className="text-red-500">
              {state?.error.company.email.message}
            </span>
          )}
        </label>
        <br />
        <label htmlFor="company_about">
          <span>about your company</span>
          <input name="company.about" type="text" />
          {state?.error?.company?.about && (
            <span className="text-red-500">
              {state?.error.company.about.message}
            </span>
          )}
        </label>
        <br />
        <label htmlFor="company_twitter">
          <span>Twitter</span>
          <input name="company.twitter" type="text" />
          {state?.error?.company?.twitter && (
            <span className="text-red-500">
              {state?.error.company.twitter.message}
            </span>
          )}
        </label>
        <br />
        <label htmlFor="company_linkedin">
          <span>Linkedin</span>
          <input name="company.linkedin" type="text" />
          {state?.error?.company?.linkedin && (
            <span className="text-red-500">
              {state?.error.company.linkedin.message}
            </span>
          )}
        </label>
        <br />
        <label htmlFor="company_instagram">
          <span>Instagram</span>
          <input name="company.instagram" type="text" />
          {state?.error?.company?.instagram && (
            <span className="text-red-500">
              {state?.error.company.instagram.message}
            </span>
          )}
        </label>
        <br />
        <br />
        <br />
        <SubmitJobListingButton />
      </div>
      {state?.submitError && <p>{state.message}</p>}
    </form>
  );
}
