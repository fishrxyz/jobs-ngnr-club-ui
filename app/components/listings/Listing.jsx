import { notFound } from "next/navigation";
import getListingById from "../../../lib/getListingById";

const Listing = async ({ listingId }) => {
  const { data: job } = await getListingById(listingId);

  return (
    <div>
      <h1>{job.title}</h1>
      <p>Remote: {job.remote}</p>
      <p>Location: {job.location}</p>
      <p>Category: {job.category}</p>
      <p>
        Salary Range: {job.salary_min} - {job.salary_max}
      </p>
      <p>About the role: {job.description}</p>
      <sidebar>
        <p>Company Logo</p>
        <p>{job.company.name}</p>
        <p>Size: {job.company.size}</p>
        <p>Website: {job.company.website}</p>
        <p>Twitter: {job.company.twitter}</p>
        <p>Instagram: {job.company.instagram}</p>
        <p>Linkedin: {job.company.linkedin}</p>
      </sidebar>
    </div>
  );
};

export default Listing;
