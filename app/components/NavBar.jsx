"use client";

import ThemeToggle from "./ThemeToggle";

const NavBar = () => {
  return (
    <nav className="flex items-center justify-between">
      <ThemeToggle />
    </nav>
  );
};

export default NavBar;
