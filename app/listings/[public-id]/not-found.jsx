"use client";

import React from "react";

export default function NotFound() {
  return <div className="text-content"> Listing not found.</div>;
}
