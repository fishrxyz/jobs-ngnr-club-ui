import React, { Suspense } from "react";

import Listing from "../../components/listings/Listing";
import { ErrorBoundary } from "react-error-boundary";

export default async function GetJobListing({ params }) {
  const jobListingID = params["public-id"];

  return (
    <div className="text-content">
      <h1>Job Listing hehe ...</h1>
      <Suspense fallback={<h2>Loading ...</h2>}>
        <Listing listingId={jobListingID} />
      </Suspense>
    </div>
  );
}
