"use client";

import React from "react";

export default function ServerError({ error }) {
  return <div className="text-content">An error occured server-side.</div>;
}
