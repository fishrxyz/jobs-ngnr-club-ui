"use server";
import { revalidateTag } from "next/cache";
import createNewListing from "../../../lib/createNewListing";
import NewListingSchema from "../../../lib/schema/NewListing";
import { prettifyErrors } from "../../../lib/util";
import { TAG_ALL_LISTINGS } from "../../../lib/constants";

const createJobListing = async (state, data) => {
  const result = NewListingSchema.safeParse({
    title: data.get("title"),
    location: data.get("location"),
    remote: data.has("remote") ? true : false,
    pinned: data.has("pinned") ? true : false,
    category: data.get("category"),
    contract_type: data.get("contract_type"),
    description: data.get("description"),
    posting_url: data.get("posting_url"),
    salary_min: data.get("salary_min"),
    salary_max: data.get("salary_max"),
    company: {
      name: data.get("company.name"),
      size: data.get("company.size"),
      email: data.get("company.email"),
      about: data.get("company.about"),
      twitter: data.get("company.twitter"),
      instagram: data.get("company.instagram"),
      linkedin: data.get("company.linkedin"),
    },
  });

  if (result.success) {
    const requestBody = JSON.parse(JSON.stringify(result.data));
    const res = await createNewListing(requestBody);

    if (!res.ok) {
      const message = res.data?.message || "An error occured";
      return { submitError: true, message };
    }

    revalidateTag(TAG_ALL_LISTINGS);
    return { error: false, success: true };
  }

  if (result.error) {
    const formattedErrors = result.error.format();
    const prettyErrors = prettifyErrors(formattedErrors);

    return { error: prettyErrors, success: false };
  }
};

export default createJobListing;
