import Link from "next/link.js";
import getAllListings from "../lib/getAllListings.jsx";

export default async function Home() {
  const res = await getAllListings();
  const jobListings = res.data;

  return (
    <main className="p-24 text-content">
      {jobListings.map((job, idx) => (
        <div key={idx} className="mb-20">
          <h3>
            <Link href={`/listings/${job.public_id}`}>{job.title}</Link>
          </h3>
          <p>Location: {job.location}</p>
          <p>Pinned: {job.pinned}</p>
          <p>Remote: {job.remote}</p>
          <p>Type: {job.contract_type}</p>
          <p>
            salary range: {job.salary_min} - {job.salary_max}
          </p>
          <p>Category: {job.category}</p>
          <p>Posted on: {job.created_at}</p>
          <p>Company: {job.company.name} </p>
        </div>
      ))}
    </main>
  );
}
